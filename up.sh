#/bin/bash
set -x
set -e

git pull --rebase
pip install -r requirments.txt
cd my
python manage.py migrate
python manage.py collectstatic --noinput
python manage.py check --deploy
