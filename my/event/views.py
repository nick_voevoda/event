from django.shortcuts import render, redirect
from .models import Article, Comments
from django.http import Http404
from django.core.exceptions import ObjectDoesNotExist
from .forms import CommentForm
from django.core.context_processors import csrf
from django.contrib import auth

def home(request):
    return render(request, 'event/home.html', {})


def about(request):
    return render(request, 'event/about.html', {})


def articles(request):
    return render(request, 'event/special.html', {'articles': Article.objects.all(),
                                                  'username': auth.get_user(request).username})


# def article(request, article_id=1):
#     return render(request, 'event/special.html', {'article': Article.objects.get(id=article_id),
#                                                   'comments': Comments.objects.filter(comment_article_id=article_id)})

def article(request, article_id=1):
    comment_form = CommentForm
    args = {}
    args.update(csrf(request))
    args['article'] = Article.objects.get(id=article_id)
    args['comments'] = Comments.objects.filter(comment_article_id=article_id)
    args['form'] = comment_form
    args['username'] = auth.get_user(request).username
    return render(request, 'event/special.html', args)


def addlike(request, article_id):
    try:
        if article_id in request.COOKIES:
            redirect('/special/')
        else:
            article = Article.objects.get(id=article_id)
            article.article_likes += 1
            article.save()
            response = redirect('/special/')
            response.set_cookie(article_id, 'test')
            return response
    except ObjectDoesNotExist:
        raise Http404
    return redirect('/special/')


def addcomment(request, article_id):
    if request.POST and ('pause' not in request.session):
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.comments_article = Article.objects.get(id=article_id)
            form.save()
            request.session.set_expiry(60)
            request.sesseion['pause'] = True
    return redirect('/special/get/%s/' % article_id)