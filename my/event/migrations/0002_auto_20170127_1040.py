# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('article_title', models.CharField(max_length=200)),
                ('article_text', models.TextField()),
                ('article_date', models.DateTimeField()),
                ('article_likes', models.IntegerField()),
            ],
            options={
                'db_table': 'article',
            },
        ),
        migrations.RemoveField(
            model_name='forumuser',
            name='user',
        ),
        migrations.RemoveField(
            model_name='topic',
            name='author',
        ),
        migrations.DeleteModel(
            name='ForumUser',
        ),
        migrations.DeleteModel(
            name='Topic',
        ),
    ]
