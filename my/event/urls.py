from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.home, name='home'),
    url(r'^about/$', views.about, name='about'),


    url(r'^special/$', views.articles, name='special'),
    url(r'^special/get/(?P<article_id>\d+)/$', views.article),
    url(r'^special/addlike/(?P<article_id>\d+)/$', views.addlike),
    url(r'^special/addcomment/(?P<article_id>\d+)/$', views.addcomment),
]