from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', views.loginuser, name='login'),
url(r'^logout/$', views.logout, name='logout'),

]